
db.fruits.aggregate([
  
    {$match: {$and:[{supplier:"Yellow Farms"},{price:{$lt:50}}]}},
    {$count: "totalItemsForYellowLessThan50"}
        
])

db.fruits.aggregate([
  
    {$match: {price:{$lt:30}}},
    {$count: "itemsLessThan30"}
        
])
    
db.fruits.aggregate([

    {$match: {supplier:"Yellow Farms"}},
    {$group: {_id:"Yellow Farms", avgPrice: {$avg: "$price"}}}

])

db.fruits.aggregate([

    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"Red Farms Inc.", highestPrice: {$max: "$price"}}}

])

db.fruits.aggregate([

    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"Red Farms Inc.", lowestPrice: {$min: "$price"}}}

])
