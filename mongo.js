db.fruits.insertMany(

	[
    	{
    		"name": "Apple",
	        "supplier": "Red Farms Inc.",
	        "stocks": 20,
	        "price": 40,
	        "onSale": true

      	},
      	{

	        "name": "Banana",
	        "supplier": "Yellow Farms",
	        "stocks": 15,
	        "price": 20,
	        "onSale": true

      	},
      	{

	        "name": "Kiwi",
	        "supplier": "Green Farming and Canning",
	        "stocks": 25,
	        "price": 50,
	        "onSale": true

      	},
      	{

	        "name": "Mango",
	        "supplier": "Yellow Farms",
	        "stocks": 10,
	        "price": 60,
	        "onSale": true

      	},
      	{

	        "name": "Dragon Fruit",
	        "supplier": "Red Farms Inc.",
	        "stocks": 10,
	        "price": 60,
	        "onSale": true

      	},
    ]

)


/*
	Aggregation Pipeline Stages

	Aggregation is typically done in 2-3 steps. Each process in aggregation is called a stage.

	$match - is used to match or get documents which satisfies the condition
	//syntax: {$match: {field:<value>}}

	$group - allows us to group together documents and create an analysis out of the grouped documents.
	_id: in the group stage, essentially associates an id to our results
	_id: also determines the number of groups.

	_id: "$supplier" - essentially group together documents with the same values in the supplier field.

*/


db.fruits.aggregate([

	//looked for and got all fruits that are onSale.
	{$match: {onSale:true}}, //apple, banana, mango, kiwi, dragon fruit

	/*
		apple
		supplier: Red Farms Inc

		banana
		supplier: Yellow Farms

		mango
		supplier: Yellow Farms

		kiwi
		supplier: Green Farming

		dragon fruit:
		supplier: Red Farms Inc

		group1
		_id: Red Farms Inc
		apple
		stocks:20
		dragon fruit
		stocks:10
		$sum: 30

		group2
		_id: Yellow Farms
		banana
		stocks:15
		mango
		stocks:10
		$sum: 25

		group3
		_id: Green Farming
		kiwi
		stocks:25
		$sum: 25

	*/

	{$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}


])


db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null,totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"AllOnSaleFruits",totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

	{$match: {supplier:"Red Farms Inc."}},
	{$group: {_id:"Red Farm",totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

	{$match: {supplier:"Yellow Farms"}},
	{$group: {_id:"Yellow Farms",totalStocks:{$sum:"$stocks"}}}

])


db.fruits.aggregate([

	{$match: {$and:[{onSale:true},{supplier:"Yellow Farms"}]}},
	{$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}

])

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"$supplier", avgStock: {$avg:"$stocks"}}}

])

db.fruits.aggregate([

	{$match:{}},
	{$group:{_id:"$supplier", avgPrice: {$avg:"$price"}}}
	
])

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:null, avgPrice: {$avg:"$price"}}}
	
])


//$max - will allow us to get the higher value out of all the values in a given field per group.

//highest number of stock for all items on sale
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id: "highestStockOnSale", maxStock: {$max: "$stocks"}}}

])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null, maxPrice: {$max: "$price"}}}

])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"lowestStockOnSale", minStock: {$min: "$stocks"}}}

])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"lowestPriceOnSale", minPrice: {$min: "$price"}}}

])

db.fruits.aggregate([

	{$match: {price:{$lt:50}}},
	{$group: {_id:"all", stock: {$min: "$stock"}}}

])

db.fruits.aggregate([
  
    {$match: {onSale:true}},
    {$count: "itemsOnSale"}
        
])
    
db.fruits.aggregate([
  
    {$match: {price:{$lt:50}}},
    {$count: "itemsPriceLessThan50"}
        
])
    
db.fruits.aggregate([
  
    {$match: {stocks:{$lt:20}}},
    {$count: "forRestock"}
        
])  

//$out - save/output the results in a new collection
//note: This will overwrite the collection if it already exists

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"$supplier", avgPrice:{$avg:"$price"}}},
	{$out: "stocksPerSupplier"}

])

